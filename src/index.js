import Resolver from '@forge/resolver';

const resolver = new Resolver();

resolver.define('getText', (req) => {
  console.log(req);

  return 'Hello world';
});

resolver.define('storeData', (req) => {
  console.log(req);

  return 'Saved!';
});

export const handler = resolver.getDefinitions();
